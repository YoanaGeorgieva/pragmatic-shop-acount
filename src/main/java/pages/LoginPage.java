package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utils.Browser;

public class LoginPage {

    private static final By LOC_MYACOUNT = By.cssSelector(".fa-user");
    private static final By LOC_REGISTER = By.xpath("//a[@href=\"http://shop.pragmatic.bg/index.php?route=account/register\"]");
    private static final By LOC_FIRST_NAME = By.id("input-firstname");
    private static final By LOC_LAST_NAME = By.id("input-lastname");
    private static final By LOC_EMAIL = By.id("input-email");
    private static final By LOC_TELEPHONE = By.id("input-email");
    private static final By LOC_PASSWORD = By.id("input-password");
    private static final By LOC_CONFIRM_PASS = By.id("input-confirm");
    private static final By LOC_ERROR_MESSAGE = By.cssSelector(".alert-dismissible");

    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg/");

    }
    public static void goToLoginPage(){

        Browser.driver.findElement(LOC_MYACOUNT).click();
        Browser.driver.findElement(LOC_REGISTER).click();
    }

    public static void login(String firstName, String lastName, String email,
                             String telephone, String password,String confirmPassword) {
        Browser.driver.findElement(LOC_FIRST_NAME).sendKeys(firstName);
        Browser.driver.findElement(LOC_LAST_NAME).sendKeys(lastName);
        Browser.driver.findElement(LOC_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOC_TELEPHONE).sendKeys(telephone);
        Browser.driver.findElement(LOC_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_CONFIRM_PASS).sendKeys(confirmPassword);

    }

    public static void radiobutton() {
        WebElement yesButton = Browser.driver.findElement(By.xpath("//input[@value=\"1\"]"));
        if (!yesButton.isSelected())
            yesButton.click();
    }

    public static void checkbox(){
        WebElement checkButton = Browser.driver.findElement(By.xpath("//input[@name=\"agree\"]"));
        if (!checkButton.isSelected())
            checkButton.click();
    }
    public static void continuebutton () {
        Browser.driver.findElement(By.cssSelector(".btn-primary")).click();
    }

    public static void errorValidationMessage(String expectedMessage, String messageInCaseOfIncorrectValidationMessage) {
        String actualErrorValidationMessage = Browser.driver.findElement(LOC_ERROR_MESSAGE).getText();

        Assert.assertTrue(actualErrorValidationMessage.contains(expectedMessage), messageInCaseOfIncorrectValidationMessage);
    }




}
