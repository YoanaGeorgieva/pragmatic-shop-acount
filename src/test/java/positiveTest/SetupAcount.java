package positiveTest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.AssertVerifications;
import utils.Browser;

public class SetupAcount {

    @BeforeMethod
    public void setUp(){
        Browser.open();
    }

    @Test
    public void loginacount() {
        LoginPage.goTo();
        LoginPage.goToLoginPage();
        AssertVerifications.verifyTitle("Register Account", "not such title found");

        LoginPage.login("Yoana","Georgieva",
                "y.j.georgieva@gmail.com", "888956431","1q2w3e4r5t", "1q2w3e4r5t");
        LoginPage.checkbox();
        LoginPage.radiobutton();
        LoginPage.continuebutton();




    }

    @AfterMethod
    public void tearDown(){
        Browser.driver.close();
    }

}
